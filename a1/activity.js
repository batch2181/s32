const http = require('http');

const port = 4000;

const server = http.createServer(function(req, res) {

	if(req.url == '/profile' && req.method == 'GET') {
		res.writeHead(200, {'Content-Type': 'text/plain'})
		res.end('Welcome to your profile')
	} else if (req.url == '/courses' && req.method == 'GET') {
		res.writeHead(200, {'Content-Type': 'text/plain'})
		res.end("Here's our courses available")
	} else if (req.url == '/addCourse' && req.method == 'POST') {
		res.writeHead(200, {'Content-Type': 'text/plain'})
		res.end('Add course to our resources')
	} else if (req.url == '/' && req.method == 'GET') {
		res.writeHead(200, {'Content-Type': 'text/plain'})
		res.end('Welcome to booking system')
	} else if (req.url == '/updateCourse' && req.method == 'PUT') {
		res.writeHead(200, {'Content-Type': 'text/plain'})
		res.end('Update a course to our resources')
	} else if (req.url == '/archiveCourse' && req.method == 'DELETE') {
		res.writeHead(200, {'Content-Type': 'text/plain'})
		res.end('Archive courses to our resources')
	} 
});

server.listen(port);

console.log(`Server is now accessible at localhost: ${port}`);


// 1. What directive is used by Node.js in loading the modules it needs?
// Answer: require()


// 2. What Node.js module contains a method for server creation?

// Answer: HTTP module



// 3. What is the method of the http object responsible for creating a server using Node.js?

// Answer: createServer()


// 4. What method of the response object allows us to set status codes and content types?

// Answer: writeHead()


// 5. Where will console.log() output its contents when run in Node.js?

// Answer: Terminal

// 6. What property of the request object contains the address's endpoint?

// Answer: url



